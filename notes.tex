\documentclass[a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{geometry}

\usepackage[francais]{babel}
\usepackage{babelbib}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amscd}
\usepackage{anyfontsize}
\usepackage{booktabs}
\usepackage[american, smartlabels]{circuitikz}
\usepackage{color}
\usepackage{framed}
\usepackage{graphicx}
\usepackage[unicode=true]{hyperref}
\usepackage[all]{hypcap} % Keep after hyperref
\usepackage{latexsym}
\usepackage{mathpazo}
\usepackage{mathtools}
\usepackage{pgfplots}
\usepackage{placeins}
\usepackage{siunitx}
\sisetup{locale = FR}
\usepackage{subfigure}
\usepackage{tikz}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{decorations.markings}
\usepackage{textcomp}
\usepackage{xcolor}

% https://tex.stackexchange.com/questions/2644/how-to-prevent-a-page-break-before-an-itemize-list#2645
\makeatletter
\newcommand\nobreakbeforeitemize{\par\nobreak\@afterheading}
\makeatother

\title{Laboratoire Franck \& Hertz}
\author{Louis Moureaux \\[.5em] \texttt{lmoureau@ulb.ac.be}}
\date{2020}

\begin{document}

\maketitle

L'expérience de Franck \& Hertz a marqué le début du \textsc{xix}$^\text{e}$
siècle par la mise en évidence du caractère discret des niveaux d'énergie des
électrons dans les atomes. Cette découverte, qui a grandement contribué au
développement de la mécanique quantique, a valu le prix Nobel 1925 à ses
auteurs. Dans ce laboratoire, nous vous proposons de reproduire l'expérience
originale avec des moyens modernes.
L'interprétation originale de Franck et Hertz présentée ici a depuis été
contestée; vous trouverez plus de détails dans les références
\cite{rapior2006new} et \cite{robson2014one}.

\section{Cadre théorique}

Dans un atome, les électrons sont organisés de manière bien précise: ils se
répartissent sur des \textit{orbitales}, qu'ils remplissent dans un ordre
défini~\cite{chimf101}. Les 80 électrons d'un atome de mercure sont ainsi
répartis comme suit:
\[\mathrm{
    (1s^2 \: % He
    2s^2 \: 2p^6 \: % Ne
    3s^2 \: 3p^6 \: % Ar
    3d^{10} \: 4s^2 \: 4p^6 \: % Kr
    4d^{10} \: 5s^2 \: 5p^6) \: % Xe
    4f^{14} \: 5d^{10} \: 6s^2 \: % Hg
    6p^0 \: 7s^0 \: \dots
}\]
Notons que seules les orbitales les plus à gauche sont remplies. Cette
répartition s'explique par le fait qu'une énergie potentielle est associée à
chacune d'entre elles, et que les électrons remplissent préférentiellement les
orbitales les moins énergétiques. Pour que le remplissage s'effectue de gauche à
droite, on ordonne généralement les orbitales par énergie croissante:
\[
    E_\mathrm{1s} <
    E_\mathrm{2s} <
    E_\mathrm{2p} <
    E_\mathrm{3s} <
    E_\mathrm{3p} <
    E_\mathrm{3d} <
    E_\mathrm{4s} <
    E_\mathrm{4p} <
    \dots
\]

Cette vision idéalisée n'est cependant pas entièrement correcte: il est en effet
possible de fournir de l'énergie à un électron pour le faire remonter sur une
orbitale plus énergétique. Par exemple, pour l'atome de mercure:
\[\mathrm{
    4f^{14} \: 5d^{10} \: 6s^2
    \xrightarrow{\SI{4.7}{\electronvolt}}
    4f^{14} \: 5d^{10} \: 6s^1 \: 6p^1
}\]
Dans certains cas, un atome ainsi excité peut revenir spontanément à son état
fondamental en réémettant l'énergie reçue sous la forme d'un photon (une
particule de lumière), généralement dans le visible ou l'ultraviolet proche.
La figure~\ref{fig:niveaux-mercure} reprend quelques-uns
des principaux niveaux d'énergie de l'atome de mercure.

Chaque orbitale est en réalité divisée en plusieurs niveaux d'énergie,
distingués par deux indices. Ainsi, l'orbitale $6\mathrm{p}$ du mercure se
divise en quatre niveaux:
\[\mathrm{
    6^3p_0, \quad
    6^3p_1, \quad
    6^3p_2 \quad\text{et}\quad
    6^1p_1.
}\]
La signification des indices est liée au \textit{spin} de
l'atome~\cite{physf306}.

Si on fournit suffisamment d'énergie à un électron d'un atome, celui-ci peut
s'échapper, suivant la réaction $\mathrm{A}+E\to\mathrm{A}^+ + e^-$. On parle
alors d'ionisation; l'énergie minimum à apporter pour que ce processus ait lieu
est appelée \emph{énergie d'ionisation}.

\begin{figure}
    \centering
    \includegraphics{images/niveaux-mercure.pdf}
    \caption{
        Diagramme représentant quelques niveaux d'énergie du mercure
        \cite{niveaux-mercure}. L'axe vertical correspond à l'énergie nécessaire
        pour atteindre un niveau depuis l'état fondamental, en
        \si{\electronvolt}.
    }
    \label{fig:niveaux-mercure}
\end{figure}

\section{L'expérience de Franck \& Hertz}

L'expérience de Franck \&
Hertz~\cite{youtube-franck-hertz,wikipedia-franck-hertz} permet l'étude des
collisions entre des électrons et des atomes de mercure. Lors d'une telle
rencontre, plusieurs processus peuvent avoir lieu:
\begin{enumerate}
    \item Lors de collisions élastiques, $\mathrm{Hg}+e^-\to\mathrm{Hg}+e^-$,
        les électrons sont déviés sans perdre d'énergie;
        \label{collision}
    \item L'électron incident peut également céder une partie de son énergie à
        l'un des électrons de l'atome de mercure, lui permettant d'atteindre une
        orbitale plus énergétique. Cette réaction, $\mathrm{Hg}+e^-\to
        \mathrm{Hg^*}+e^-$, est un exemple de collision inélastique;
        \label{excitation}
    \item Selon le processus inverse, un électron peut rencontrer un atome
        excité $\mathrm{Hg^*}$, qui lui transmet alors son énergie:
        $\mathrm{Hg}^*+e^-\to\mathrm{Hg}+e^-$;
        \label{desexcitation}
    \item Enfin, s'il a une énergie suffisante, l'électron incident peut ioniser
        l'atome de mercure: $\mathrm{Hg}+e^-\to\mathrm{Hg^+}+2e^-$.
        \label{ionisation}
\end{enumerate}
C'est l'observation du processus \ref{excitation} qui a valu leur prix Nobel à
Franck et Hertz.

\begin{figure}
    \centering
    \begin{subfigure}[]
        \centering
        \begin{tikzpicture}
            \draw node{\includegraphics[width=0.5\textwidth]{images/tube.jpeg}};
            \draw (3.92, -1.35) node[rotate=90,font=\tiny]{Photo: Y. Allard};
            \draw (3.92, -1.043)
                node[font={\fontsize{0.4}{0.5}\selectfont},white]{(the best)};
        \end{tikzpicture}
    \end{subfigure}%
    \quad%
    \begin{subfigure}[]
        \centering
        \begin{tikzpicture}
            \begin{scope}[
                    z={(0, 1)},
                    y={(0.9, -0.25)},
                    x={(-0.9, 0, -0.25)},
                ]

                % Anode
                \filldraw[fill=white,thick] (0, 0)
                    -- ++(0, 0, 1.5) -- ++(0, 1, 0) -- ++(0, 0, -1.5) -- cycle;
                \draw (0, 0, 1.5) node[above]{$\mathcal A$};
                \draw (0, 0.8, 0) -- (0, 0.8, -0.5);

                % Électron
                \draw[dashed] (0.5, 0.5, 0.75) -- ++(-0.5, 0, 0);

                % Grille 2
                \draw[thick] (0.5, 0)
                    -- ++(0, 0, 1.5) -- ++(0, 1, 0) -- ++(0, 0, -1.5) -- cycle;
                \foreach \i in {1, 2, ..., 14} {
                    \draw[very thin] (0.5, 0) ++(0, 0, {1.5 * \i / 15}) -- ++(0, 1, 0);
                }
                \foreach \i in {1, 2, ..., 9} {
                    \draw[very thin] (0.5, 0) ++(0, {\i / 10}, 0) -- ++(0, 0, 1.5);
                }
                \draw (0.5, 0, 1.5) node[above=-2]{$\mathcal G_2$};
                \draw (0.5, 0.8, 0) -- (0.5, 0.8, -0.5);

                % Électron
                \draw[
                        dashed,
                        postaction={decorate},
                        decoration={
                                markings,
                                mark={at position 0.55 with {\arrow{Stealth}}}
                            },
                    ]
                    (2, 0.5, 0.75) -- ++(-1.5, 0, 0);

                % Grille 1
                \draw[thick] (2, 0)
                    -- ++(0, 0, 1.5) -- ++(0, 1, 0) -- ++(0, 0, -1.5) -- cycle;
                \foreach \i in {1, 2, ..., 14} {
                    \draw[very thin] (2, 0) ++(0, 0, {1.5 * \i / 15}) -- ++(0, 1, 0);
                }
                \foreach \i in {1, 2, ..., 9} {
                    \draw[very thin] (2, 0) ++(0, {\i / 10}, 0) -- ++(0, 0, 1.5);
                }
                \draw (2, 0, 1.5) node[above=-2]{$\mathcal G_1$};
                \draw (2, 0.8, 0) -- (2, 0.8, -0.5);

                % Cathode
                \filldraw[fill=white,thick] (2.5, 0)
                    -- ++(0, 0, 1.5) -- ++(0, 1, 0) -- ++(0, 0, -1.5) -- cycle;
                \draw (2.5, 0, 1.5) node[above]{$\mathcal K$};
                \draw (2.5, 0.8, 0) -- (2.5, 0.8, -0.5);

                % Filament
                \draw[
                        thick,
                        decorate,
                        decoration={
                            zigzag,
                            amplitude=1pt,
                            segment length=0.1cm,
                        },
                    ]
                    (2.55, 0.25, 0.75) -- ++(0, 0.5, 0)
                        node[midway, above]{$\mathcal F$};
                \draw (2.55, 0.75, 0.75) -- ++(0, 0.05, 0) -- ++(0, 0, -0.2)
                    -- ++(0, -0.4, 0) -- (2.55, 0.4, -0.5);
                \draw (2.55, 0.25, 0.75) -- (2.55, 0.2, 0.75) -- (2.55, 0.2, -0.5);
            \end{scope}

            \draw (2.5, 0.25) node[font=\small,align=left]{
                \setlength{\tabcolsep}{3pt}
                \begin{tabular}{ll}
                    \multicolumn{2}{l}{\textbf{Légende}} \\[2pt]
                    $\mathcal K$    & Cathode \\
                    $\mathcal G_1$  & Grille 1 \\
                    $\mathcal G_2$  & Grille 2 \\
                    $\mathcal A$    & Anode \\
                    $\mathcal F$    & Filament \\
                \end{tabular}
            };
        \end{tikzpicture}
    \end{subfigure}
    \caption{%
        Tube utilisé au laboratoire (a) et disposition des électrodes (b).
    }
    \label{fig:tube}
\end{figure}

La manipulation s'effectue avec du mercure gazeux à basse pression, enfermé dans
un tube hermétique~\cite{tube-mercure}. Quatre électrodes planes parallèles (une
cathode, deux grilles et une anode, figure~\ref{fig:tube}) sont placées au sein
du tube et permettent d'y créer trois champs électriques uniformes. La cathode
est portée à haute température par une résistance chauffante (aussi appelée
filament), ce qui lui permet d'émettre des électrons. Ceux-ci sont accélérés en
direction de l'anode à l'aide de champs électriques imposés entre les grilles.
Le courant produit sur l'anode
dépend des interactions subies par les électrons au cours de leur trajet, et le
mesurer permet donc d'étudier celles-ci.

Les différents processus contribuent au courant total de la manière suivante:
\nobreakbeforeitemize
\begin{enumerate}
    \item Le processus~\ref{collision} permet d'assimiler le gaz à une
        résistance, avec une relation grossièrement linéaire entre la différence
        de potentiel et l'intensité du courant, $V\approx RI$;
    \item L'excitation des atomes ne peut avoir lieu que si les électrons ont
        acquis assez d'énergie pour ce faire (grâce au champ électrique entre
        les grilles). Quand ils excitent un atome, les électrons lui transfèrent
        leur énergie cinétique et s'arrêtent, ce qui diminue le courant;
    \item La désexcitation des atomes préalablement excités $\mathrm{Hg}^*$
        transmet de l'énergie à l'électron et augmente sa vitesse, et donc le
        courant;
    \item Lors de l'ionisation d'un atome, on passe d'une seule particule
        chargée à trois. Si le champ électrique est assez important pour à
        nouveau accélérer celles-ci, elles peuvent à nouveau entrer en
        collision, ce qui crée une avalanche. Le résultat est une variation
        très rapide du courant avec la tension.
\end{enumerate}
Les processus~\ref{excitation} et~\ref{desexcitation} s'opposent, ce qui en
l'absence d'une réaction supplémentaire donnerait lieu à un équilibre chimique
$\mathrm{Hg}\overset{e}{\rightleftharpoons}\mathrm{Hg}^*$, neutre du point de
vue du courant. Seules les orbitales pour lesquelles la désexcitation par
émission d'un photon ($\mathrm{Hg}^*\to\mathrm{Hg}+\gamma$) est possible donnent
lieu à une variation du courant, les photons pouvant s'échapper à travers la
paroi du tube et emporter de l'énergie hors du système.

Suivant la configuration des champs utilisée, on peut mettre en évidence les
processus~\ref{excitation} ou~\ref{ionisation}. Les montages correspondants
sont décrits plus en détail ci-dessous.

\subsection{Ionisation}\label{sec:montage-ionisation}

La mise en évidence de l'ionisation des atomes de mercure peut s'effectuer à
l'aide du montage de gauche de la figure~\ref{fig:circuits}. Celui-ci permet
d'obtenir dans le tube la configuration de potentiels représentée sur la
figure~\ref{fig:potentiels}, qui crée un champ électrique et permet donc
d'accélérer les électrons. Attardons-nous brièvement sur leur parcours:
\nobreakbeforeitemize
\begin{itemize}
    \item Entre la cathode et la première grille, les électrons passent d'un
        potentiel nul à un potentiel $V_V>0$; ils gagnent donc une énergie
        $E=eV_V$, où $e$ est la charge électrique d'un électron (en valeur
        absolue);
    \item Entre les deux grilles, le potentiel est constant, ce qui correspond à
        un champ nul. L'énergie des électrons ne peut changer que par leur
        interaction avec le gaz;
    \item Comme l'anode est à un potentiel inférieur à celui de la cathode, les
        électrons ne peuvent jamais l'atteindre: ils sont stoppés après la
        seconde grille. Par contre, d'éventuelles charges positives sont
        attirées par l'anode et peuvent donc y créer un courant.
\end{itemize}

\begin{figure}
    \centering
    \begin{subfigure}[]
        \centering
        \begin{circuitikz}
            \draw (0, 0) node[coordinate](ground){};
            \draw (2.8, 3) node[ocirc]{} node[above]{$\mathcal A$}
                to[twoport, t={\si{\ampere}}]
                ++(0, -1.5) to[V, invert, l_={$S_2(V_2)$}]
                ++(0, -1.5) --
                (ground);
            \draw (ground) to[V, invert, l={$S_V(V_V)$}]
                ++(0, 1.5) --
                ++(0, 0.5) node[coordinate](sV){} --
                ++(1.5, 0) --
                ++(0, 1) node[ocirc]{} node[above]{$\mathcal G_2$};
            \draw (sV) --
                ++(-1.5, 0) --
                ++(0, 1) node[ocirc]{} node[above]{$\mathcal G_1$};
            \draw (ground) --
                ++(-2.8, 0) --
                ++(0, 3) node[ocirc]{} node[above]{$\mathcal K$};
        \end{circuitikz}
    \end{subfigure}
    \qquad\qquad
    \begin{subfigure}[]
        \centering
        \begin{circuitikz}
            \draw (0, 0) node[coordinate](g1){};
            \draw (g1) --
                ++(0, 2.5) node[ocirc]{} node[above]{$\mathcal G_1$};
            \draw (g1) to[V, invert, l={$S_V(V_V)$}]
                ++(3, 0) node[coordinate](g2){} --
                ++(0, 2.5) node[ocirc]{} node[above]{$\mathcal G_2$};
            \draw (-1.3, 2.5) node[ocirc]{} node[above]{$\mathcal K$} --
                ++(0, -2.5) to[V, invert, l={$S_2(V_2)$}]
                (g1);
            \draw (4.3, 2.5) node[ocirc]{} node[above]{$\mathcal A$}
                to[twoport, t={\si{\ampere}}]
                ++(0, -1.5) --
                ++(0, -1) to[V, invert, l_={$S_3(V_3)$}]
                (g2);
        \end{circuitikz}
    \end{subfigure}
    \caption{%
        Montages électriques permettant d'observer (a) l'ionisation des atomes
        de mercure et (b) leur premier niveau d'excitation. $S_V$ est une source
        de tension variable, tandis que $S_2$ et $S_3$ restent fixes tout au
        long de l'expérience.
    }
    \label{fig:circuits}
\end{figure}

\begin{figure}
    \begin{subfigure}[]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                    width=7cm,
                    height=5.5cm,
                    xlabel=Position dans le tube,
                    xmin=0,
                    xmax=5,
                    xtick={0, 1, 4, 5},
                    xticklabels={
                        $\mathcal K$,
                        $\mathcal G_1$,
                        $\mathcal G_2$,
                        $\mathcal A$},
                    xlabel style={overlay},
                    ylabel=Potentiel électrostatique,
                    ymin=-1.5,
                    ymax=6,
                    ytick={-1, 0, 5},
                    yticklabels={
                        $-V_2$,
                        $0$,
                        $V_V$},
                    every axis y label/.style={
                        at={(ticklabel cs:0.5)},
                        rotate=90,
                        anchor=south,
                    },
                    grid=major,
                ]
                \addplot[mark=.,thick,black] coordinates{
                    (0,  0)
                    (1,  5)
                    (4,  5)
                    (5, -1)
                };
            \end{axis}
        \end{tikzpicture}
    \end{subfigure}
    \quad
    \begin{subfigure}[]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                    width=7cm,
                    height=5.5cm,
                    xlabel=Position dans le tube,
                    xmin=0,
                    xmax=5,
                    xtick={0, 1, 4, 5},
                    xticklabels={
                        $\mathcal K$,
                        $\mathcal G_1$,
                        $\mathcal G_2$,
                        $\mathcal A$},
                    xlabel style={overlay},
                    ylabel=Potentiel électrostatique,
                    ymin=-0.5,
                    ymax=7,
                    ytick={0, 1, 4, 6},
                    yticklabels={
                        $0$,
                        $V_2$,
                        $V_{\mathcal G_2}$,
                        $V_{\mathcal A}$},
                    every axis y label/.style={
                        at={(ticklabel cs:0.5)},
                        rotate=90,
                        anchor=south,
                    },
                    grid=major,
                ]
                \addplot[mark=.,thick,black] coordinates{
                    (0, 0)
                    (1, 1)
                    (4, 6)
                    (5, 4)
                };
                \draw[<->] (axis cs: 1.6, 1)
                    -- (axis cs: 1.6, 6) node[midway, left]{$V_V$};
                \draw[<->] (axis cs: 1.8, 4)
                    -- (axis cs: 1.8, 6) node[midway, right]{$V_3$};
            \end{axis}
        \end{tikzpicture}
    \end{subfigure}
%     \begin{subfigure}[]
%         \centering
%         \begin{tikzpicture}
%             \draw[thick] (0, 0) -- ++(0, 2) node[above]{$\mathcal A$};
%             \draw[->] (0.7, 0.9) -- ++(-0.4, 0) node[midway,above]{$\vec E$};
%             \draw[thick,dashed] (1, 0) -- ++(0, 2) node[above]{$\mathcal G_1$};
%             \draw (2.5, 0.9) node[above]{$\vec E=\vec 0$};
%             \draw[thick,dashed] (4, 0) -- ++(0, 2) node[above]{$\mathcal G_2$};
%             \draw[->] (4.3, 0.9) -- ++(0.4, 0) node[midway,above]{$\vec E$};
%             \draw[thick] (5, 0) -- ++(0, 2) node[above]{$\mathcal C$};
%         \end{tikzpicture}
%     \end{subfigure}
    \caption{%
        Potentiel électrostatique généré dans le tube pour les deux circuits de
        la figure~\ref{fig:circuits}. Pour (b), on montre aisément que
        $V_{\mathcal G_2} = V_2 + V_V$ et $V_{\mathcal A} = V_{\mathcal G_2} -
        V_3$.
    }
    \label{fig:potentiels}
\end{figure}

Afin d'observer l'ionisation des atomes de mercure, on fait varier le potentiel
$V_V$ et donc l'énergie atteinte par les électrons. Si cette dernière est
inférieure à l'énergie d'ionisation, on s'attend à n'observer aucun courant à
l'anode; si elle lui est supérieure, les ions $\mathrm{Hg}^+$ pourront en créer
un. La connaissance de la tension minimale $V_V$ pour produire un courant permet
donc la détermination de l'énergie de première ionisation des atomes.

\paragraph{Conditions de référence}

Cette manipulation peut s'effectuer à une température $T=\SI{130}{\celsius}$,
avec la source $S_2$ réglée sur $V_2=\SI{1.5}{\volt}$ et en faisant varier $S_V$
entre $\num{0} \le V_V < \SI{25}{\volt}$.

\subsection{Excitation}\label{sec:montage-1}

L'observation de l'excitation des atomes (processus~\ref{excitation}) s'effectue
en utilisant le montage de droite de la figure~\ref{fig:circuits}. Comme
illustré sur la figure~\ref{fig:potentiels}, le potentiel entre les deux grilles
n'est alors pas constant, ce qui crée un champ électrique dans l'espace qui les
sépare. Les électrons sont donc accélérés sur une beaucoup plus grande distance.
Une autre différence importante est que les électrons peuvent ici atteindre
l'anode: ce sont eux qui créeront le courant mesuré.

Si on fait varier la tension $V_V$, on obtient une courbe proche de celle
représentée sur la figure~\ref{fig:montage-1-resultat}. On interprète ce
résultat de la manière suivante. Quand $V_V$ est proche de zéro, le potentiel de
l'anode est inférieur à celui de la cathode, et aucun électron ne peut donc
atteindre cette dernière. Pour des tensions un peu plus grandes, on commence à
observer le comportement résistif du processus~\ref{collision}. Cependant, dès
que leur énergie est suffisante, les électrons sont arrêtés par le
processus~\ref{excitation}, ce qui diminue brutalement le courant. Si la tension
augmente encore, les électrons à l'arrêt peuvent être à nouveau accélérés, ce
qui fait remonter le courant. Le processus se répète pour former les multiples
\og creux\fg{} observés dans la courbe. L'espacement entre ceux-ci permet de
déterminer l'énergie de première excitation de l'atome de mercure.

\begin{figure}
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xmin = 0,
                xmax = 20,
                xlabel = {Tension variable $V$ (u.a.)},
                ymin = 0,
                ymax = 10,
                ylabel = {Intensité $I$ du courant à l'anode (u.a.)},
            ]
            \addplot[domain=0:20,black,dashed]
                function{-0.5 + x/2};
            \addplot[domain=0:20,black,thick,samples=100]
                function{-0.5 + x/2
                    - 1.5*exp(-0.5*(x-5)^2)
                    - 1.5*exp(-0.5*(x-10)^2)
                    - 1.5*exp(-0.5*(x-15)^2)
                    - 1.5*exp(-0.5*(x-20)^2)
                };
        \end{axis}
    \end{tikzpicture}
    \caption{%
        Illustration de la mise en évidence de l'excitation des atomes de
        mercure (voir texte). Le comportement purement résistif attendu sans
        excitation est représenté en pointillés.
    }
    \label{fig:montage-1-resultat}
\end{figure}

\paragraph{Conditions de référence}

Cette manipulation peut également s'effectuer à une température
$T=\SI{130}{\celsius}$. La source $S_2$ peut être réglée sur
$V_2=\SI{1.5}{\volt}$ la source $S_3$ sur $V_3=\SI{1}{\volt}$. On fait varier
$S_V$ entre $\num{0} \le V_V < \SI{25}{\volt}$.

\section{Travail à effectuer}

\paragraph{Attention}
Le quartz constituant le tube utilisé dans cette manipulation est sensible au
contact direct avec les mains. Évitez autant que possible de le toucher.

Il vous est demandé de fournir une mesure des énergies de premières ionisation
et excitation de l'atome de mercure, en utilisant les montages décrits
ci-dessus. En plus de ce travail \og de base\fg{}, vous devez choisir un aspect
de l'expérience et le développer. Quelques idées vous sont données dans la
section~\ref{idees}, mais vous êtes libres de les mélanger ou d'en apporter de
nouvelles, tant que vous exploitez les résultats de votre manipulation.

Quel que soit votre choix, vous devez fournir \textit{au moins} une explication
qualitative de vos observations. Quand c'est possible, essayez d'être
quantitatifs (mais n'oubliez pas alors de décrire les sources d'incertitude et
d'estimer leur impact sur le résultat final). Vous trouverez une décomposition
indicative de la note dans la table~\ref{tab:note}.

\begin{table}
    \centering
    \begin{tabular}{lc}
        \toprule
        Partie                          & Points \\
        \midrule
        Travail au laboratoire          & 2 \\
        Mise en forme du rapport        & 2 \\
        \addlinespace
        Introduction et conclusion      & 4 \\
        Analyse de base                 & 6 \\
        Développement supplémentaire    & 6 \\
        \midrule
        Total                           & 20 \\
        \bottomrule
    \end{tabular}
    \caption{Décomposition indicative de la note finale du laboratoire.}
    \label{tab:note}
\end{table}

\subsection{Idées de développement}\label{idees}

Cette section reprend quelques idées d'analyses intéressantes que vous êtes en
mesure d'effectuer. Les explications sont volontairement succinctes: à vous de
trouver l'information nécessaire pour les développer. L'assistant est bien
entendu disponible pour vous aider.

\subsubsection{Variation en température}

La densité de la vapeur de mercure présente dans le tube dépend de sa
température (formule de Clausius-Clapeyron), et affecte fortement les courbes
observées. La référence~\cite{rapior2006new} mentionne également une dépendance
en la température de la position des pics de la partie \og excitation\fg. On
peut répéter l'expérience à différentes températures et comparer les résultats
obtenus.

\subsubsection{Variation des conditions expérimentales}

Les résultats de la manipulation dépendent des tensions utilisées, qu'on peut
varier en modifiant quelques réglages. On pourra par exemple effectuer la partie
\og excitation\fg{} pour différents paramètres et interpréter les comportements
observés.

\subsubsection{Second niveau d'excitation}

En effectuant un montage approprié, il est possible d'observer le second niveau
d'excitation de l'atome de mercure. Celui-ci correspond à un électron migrant,
non plus vers l'orbitale $6^3\mathrm{p}_1$ comme pour le premier niveau
(pourquoi n'observe-t-on pas les autres orbitales $6^3\mathrm{p}$?), mais
vers l'orbitale $6^1\mathrm{p}_1$. Il n'est pas visible en utilisant le montage
de la section~\ref{sec:montage-1}, parce que les électrons n'y restent pas assez
longtemps à haute énergie.

L'observation du second niveau demande donc de réaliser un montage grâce auquel
les électrons seront accélérés sur une courte distance. Comme pour le montage de
la section~\ref{sec:montage-1}, il est nécessaire d'utiliser un potentiel
négatif entre la seconde grille et l'anode; celui-ci permet de limiter le bruit
en stoppant les électrons les moins énergétiques.

\subsubsection{Détermination plus rigoureuse de l'emplacement des creux}

La détermination de l'énergie de première ionisation proposée plus haut souffre
d'un défaut important: la détermination de la position des creux est délicate.
Paradoxalement, c'est d'autant plus le cas que le nombre de points enregistré
est grand, parce qu'on obtient alors plusieurs points au même niveau. Cette
section propose une méthode pour remédier à la situation en exploitant le
théorème de Taylor (ne vous encourez pas comme ça, je vous vois!).

Notons $f(x)$ la courbe dont on cherche un minimum local $x_0$. Comme $x_0$ est
un minimum, on sait que la dérivée de la courbe est nulle en ce point:
$\mathrm df/\mathrm dx(x_0) = 0$. Le développement de Taylor de $f$
autour de $x_0$ s'écrit donc:
\[
    f(x)
    =
    f(x_0) + 0 + \frac{1}{2} f''(x_0) (x - x_0)^2 + \mathcal O((x - x_0)^3).
\]

Si on se restreint à un petit intervalle autour de $x_0$, on peut négliger les
ordres supérieurs et se limiter à un polynôme du second degré. (Autrement dit:
de près, tout minimum local d'une fonction analytique ressemble à une parabole.)

En prenant quelques points expérimentaux autour du minimum, on peut essayer de
trouver les paramètres de la parabole correspondante et de retrouver ainsi
$x_0$. L'ajustement peut se faire en minimisant l'expression suivante (méthode
des moindres carrés):
\[
    \chi^2 \sigma^2
    =
    \sum_i \left[y_i - \frac{a}{2} (x_i-x_0)^2 - b\right]^2,
\]
où $(x_i, y_i)$ sont les points expérimentaux et $\sigma$ est l'incertitude sur
les ordonnées, qu'on considère constante.
Les paramètres $a$ et $b$ contrôlent respectivement la largeur de la courbe et
la hauteur de son minimum, $a=f(x_0)$.

Les valeurs des paramètres $a$, $b$ et $x_0$ peuvent être obtenues en utilisant
la fonction \og solveur\fg{} d'un tableur ou un logiciel d'analyse numérique
(par exemple \texttt{scipy.optimize.minimize} \cite{scipy}), voire à la main.

\bibliographystyle{babunsrt}
\bibliography{bibliography}

\end{document}
