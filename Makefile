# Parameters
LATEX ?= lualatex
LATEXFLAGS = --shell-escape --interaction=batchmode --halt-on-error

# Build a pdf from a .tex file
%.pdf: %.tex *.bib
	$(LATEX) $(LATEXFLAGS) --draftmode `basename $<`
	if grep bib `basename $< .tex`.aux; then \
	  bibtex `basename $< .tex`.aux; \
	  $(LATEX) $(LATEXFLAGS) --draftmode `basename $<`; \
	fi;
	$(LATEX) $(LATEXFLAGS) `basename $<`

# List files
SOURCES = $(wildcard *.tex)
PDFS = $(patsubst %.tex,%.pdf,$(SOURCES))

# Build everything
.PHONY: all
all: $(PDFS)

# Clean up
LEFTOVERS = $(wildcard *.log) $(wildcard *.aux) $(wildcard *.out) $(wildcard *.nav) $(wildcard *.snm) $(wildcard *.toc) $(wildcard *.vrb) $(wildcard *.bbl) $(wildcard *.blg)
.PHONY: clean
clean:
	$(RM) $(LEFTOVERS)

# Clean up and remove pdfs
cleanall: clean
	$(RM) $(PDFS)
